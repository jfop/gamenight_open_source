# Bot made by ForTimeBeing & Made better by Executor

# GameNight for the bois

import matplotlib.pyplot as plt
import discord
import datetime
import pymongo
import uuid
from tkinter import *
from pymongo import MongoClient
from datetime import timedelta

from discord.ext import commands

bot = commands.Bot(command_prefix='~')
client = discord.Client()
mongoClient = MongoClient("HIDDEN DATABASE")
db = mongoClient['GameNightBot']
voters_collection = db['voters']
results_collection = db['results']
gamelist_collection = db['gamelist']

def nextVoteDate():
    newest_result_objs = results_collection.aggregate([
        { "$sort": { "scheduledDate": -1 } },
        { "$limit": 1 }
    ])
    newest_result_obj = None
    for result_obj in newest_result_objs:
        newest_result_obj = result_obj
    if newest_result_obj == None:
        nextWednesday = datetime.datetime.today()
        while nextWednesday.weekday() != 3:
            nextWednesday += datetime.timedelta(86400000)
        return nextWednesday
    else:
        last_vote_date = newest_result_obj["scheduledDate"]
        next_vote_date = last_vote_date
        while next_vote_date.weekday() != 3 or (next_vote_date.weekday() == 3 and next_vote_date.timestamp() < datetime.datetime.now().timestamp()):
            next_vote_date += datetime.timedelta(1)
        return next_vote_date

def getCurrentVote():
    current_vote_objs = results_collection.aggregate([
        { "$match": { "scheduledDate": {"$gt": datetime.datetime.now()}}},
        { "$sort": { "scheduledDate": -1 } },
        { "$limit": 1 }
    ])
    current_vote_obj = None
    
    for result_obj in current_vote_objs:
        current_vote_obj = result_obj

    if current_vote_obj == None:
        result_data = {
            '_id': str(uuid.uuid1()),
            'scheduledDate': nextVoteDate(),
            'votes': []
        }
        new_vote_id = results_collection.insert_one(result_data).inserted_id
        current_vote_obj = results_collection.find_one({"_id":new_vote_id})
        return current_vote_obj
    else:
        return current_vote_obj


@bot.event
async def on_ready():
    print(bot.user.name + " Ready")
    print(bot.user.id)


@bot.command(pass_context=True)
async def ping(ctx):
    await bot.say(":ping_pong:")

@bot.command(pass_context=True)
async def register(ctx):
    # Find if voter has already registered
    voter_obj = voters_collection.find_one({'_id': ctx.message.author.id})
    if voter_obj == None:
        # Create voter
        voter_data = {
            '_id': ctx.message.author.id,
            'userName': ctx.message.author.name
        }
        # Save voter
        voters_collection.insert_one(voter_data)
        await bot.say("<@" + user_list_id[i] + ">, Congratulations you can now vote!")
    else:
        await bot.say("You are already registered to vote!")


@bot.command(pass_context=True)
async def reminder(ctx):

    # Get this weeks current vote
    current_vote = getCurrentVote()
    allVoters = []
    for voter in voters_collection.find():
        allVoters.append(voter["_id"])

    for vote in current_vote["votes"]:
        if vote["voterId"] in allVoters:
            allVoters.remove(vote["voterId"])
                    
    for voterId in allVoters:
        await bot.say("<@" + voterId + "> VOTE")

#Allows user to vote for the game
@bot.command(pass_context=True)
async def vote(ctx, gameid):
    # Get this weeks current vote
    current_vote = getCurrentVote()

    # Get the voter obj from the database
    voter_obj = voters_collection.find_one({'_id': ctx.message.author.id})
    if voter_obj == None:
        await bot.say("You are not registered to vote! Please Register!")
        return

    # Check if user already voted
    voteCount = 0
    for current_vote_obj in current_vote['votes']:
        if current_vote_obj['voterId'] == voter_obj['_id']:
            voteCount+=1
            if voteCount == 1:
                gamelist_obj1 = gamelist_collection.find_one({'_id': current_vote_obj['gameId']})
            if voteCount == 2:
                gamelist_obj2 = gamelist_collection.find_one({'_id': current_vote_obj['gameId']})
                await bot.say("You already voted for " + gamelist_obj1['gameName'] + " and " + gamelist_obj2['gameName'])
                return

    # Get the game obj they are voting for
    gamelist_obj = gamelist_collection.find_one({'_id': gameid})
    if gamelist_obj == None:
        await bot.say("ID does not match any games")
        return

    # Save the vote to the votes array on the results collection
    vote_obj = {
        'gameId': gamelist_obj['_id'],
        'voterId': voter_obj['_id']
    }
    results_collection.update_one({'_id': current_vote['_id']}, {"$push":{'votes':vote_obj}})
    await bot.say("You voted for: " + gamelist_obj['gameName'])

#Displays info about gamenight
@bot.command(pass_context=True)
async def gamenight(ctx):
    current_vote = getCurrentVote()
    gamelist_objs = gamelist_collection.find()

    votecount = []
    gamelist = []
    gamelistNames = []
    for gamelist_obj in gamelist_objs:
        gamelist.append(gamelist_obj["_id"])
        gamelistNames.append(gamelist_obj["gameName"])
        votecount.append(0)

    for current_vote_obj in current_vote['votes']:
        votecount[gamelist.index(current_vote_obj["gameId"])] += votecount[gamelist.index(current_vote_obj["gameId"])] + 1

    highestVote = 0
    bestGame = "No Votes Yet"
    for vote in votecount:
        if vote > highestVote:
            highestVote = vote
            bestGame = gamelistNames[votecount.index(vote)]

    #Embeds the info into discord
    emded = discord.Embed(title="GameNight", description="Games for the boys", color=0x800080)
    emded.add_field(name="Next gamenight: ", value=current_vote['scheduledDate'].strftime("%A, %b %d"),inline=True)
    emded.add_field(name="Most Voted Game: ", value=bestGame, inline=True)
    emded.add_field(name="Time: ", value="7pm",inline=True)
    await bot.say(embed=emded)

#Displays gamelist
@bot.command(pass_context=True)
async def gamelist(ctx):
    gamelist_objs = gamelist_collection.find()
    emded = discord.Embed(title="Game List", color=0x800080)
    for gamelist_obj in gamelist_objs:
        emded.add_field(name=gamelist_obj["gameName"], value=gamelist_obj["_id"], inline=True)

    await bot.say(embed=emded)

#Displays help options
@bot.command(pass_context=True)
async def h(ctx):
    emded = discord.Embed(title="Help commands", color=0x800080)
    emded.add_field(name="~gamenight", value="Displays information about gamenight", inline=False)
    emded.add_field(name="~gamelist", value="Displays list of games to vote on", inline=False)
    emded.add_field(name="~vote gameid", value="Votes for next week's game",inline=False)
    emded.add_field(name="~register", value="Registers a user to vote", inline=False)
    emded.add_field(name="~reminder", value="Displays users who haven't voted", inline=False)
    emded.add_field(name="~results", value="Displays the voting results", inline=False)
    emded.add_field(name="~h", value="Displays help commands",inline=False)
    emded.set_footer(text="Coded in python. Data displayed with: matlibplot")
    await bot.say(embed=emded)



#Plots graph
@bot.command(pass_context=True)
async def results(ctx):

    # Opens gamelist db then creates gamelist list
    current_vote = getCurrentVote()
    gamelist_objs = gamelist_collection.find()
    votecount = []
    gamelist = []
    gamelistNames = []
    for gamelist_obj in gamelist_objs:
        gamelist.append(gamelist_obj["_id"])
        gamelistNames.append(gamelist_obj["gameName"])
        votecount.append(0)

    for current_vote_obj in current_vote['votes']:
        votecount[gamelist.index(current_vote_obj["gameId"])] += votecount[gamelist.index(current_vote_obj["gameId"])] + 1

    # Removes all games that were not voted for
    data = filter(lambda x: x[1] > 0, zip(gamelistNames, votecount))

    label = []
    values = []
    for elem in data:
        label.append(elem[0])
        values.append(elem[1])

    # Plots the data
    fig = plt.figure()
    plt.pie(values,labels=label,autopct="%1.1f%%")
    plt.axis('equal')

    # Saves the graph
    fig.savefig('graph.png',bbox_inches='tight')

    # Displays the saved graph
    channel = bot.get_channel("223214038845358081")
    await bot.send_file(channel, "graph.png",content="Results")


# Opens file; returns LIST of file content
def openFile(file):
    user_list_selection = [0, 0, 0, 0, 0, 0]
    try:
        with open(file, 'r+') as f:
            counter_for_line_number = 0
            for line in f:
                user_list_selection[counter_for_line_number] = line.rstrip()
                counter_for_line_number = counter_for_line_number + 1
    except FileNotFoundError:
        f = open(file, 'w+')
        x = 0
        while x < len(user_list_selection):
            f.write("0\n")
            x = x + 1
        f.close()
    return user_list_selection
